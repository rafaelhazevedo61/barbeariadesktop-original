/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import conexao.ConnectionFactory;
import controller.TelaUsuario_2_AlterarExcluirController;
import dao.UsuariosPermissaoDAO;
import java.sql.Connection;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import model.UsuariosPermissao;

/**
 *
 * @author Rafael
 */
public class TelaUsuario_2_AlterarExcluir extends javax.swing.JFrame {

    private final TelaUsuario_2_AlterarExcluirController controller;
    
    /**
     * Creates new form TelaPrincipal
     */
    public TelaUsuario_2_AlterarExcluir() {
        initComponents();
        controller = new TelaUsuario_2_AlterarExcluirController(this);
        
        //INICIALIZAR JCOMBOBOX CLIENTE
        Connection con = ConnectionFactory.getConnection();
        UsuariosPermissaoDAO dao = new UsuariosPermissaoDAO(con);
        
        for(UsuariosPermissao as: dao.ListarPermissoesJComboBox()){
            jComboBoxPermissao.addItem(as);
        }
        
        //INICIALIZAR TABELA
        controller.tabelaUsuarios();
        
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jLabelUsuario = new javax.swing.JLabel();
        jTextFieldUsuario = new javax.swing.JTextField();
        jLabelSenha = new javax.swing.JLabel();
        jPasswordFieldSenha = new javax.swing.JPasswordField();
        jLabelPermissao = new javax.swing.JLabel();
        jComboBoxPermissao = new javax.swing.JComboBox<>();
        jLabelBairroPesquisa = new javax.swing.JLabel();
        jComboBoxPesquisa = new javax.swing.JComboBox<>();
        jTextFieldPesquisa = new javax.swing.JTextField();
        jButtonPesquisar = new javax.swing.JButton();
        jScrollPaneUsuarios = new javax.swing.JScrollPane();
        jTableUsuarios = new javax.swing.JTable();
        jButtonVoltar = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jLabelPainel = new javax.swing.JLabel();
        jLabelImagemFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelUsuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelUsuario.setForeground(new java.awt.Color(255, 255, 255));
        jLabelUsuario.setText("Usuário");
        getContentPane().add(jLabelUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 70, -1, -1));
        getContentPane().add(jTextFieldUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 60, 210, 40));

        jLabelSenha.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelSenha.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSenha.setText("Senha");
        getContentPane().add(jLabelSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 120, -1, -1));
        getContentPane().add(jPasswordFieldSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 110, 210, 40));

        jLabelPermissao.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelPermissao.setForeground(new java.awt.Color(255, 255, 255));
        jLabelPermissao.setText("Permissão");
        getContentPane().add(jLabelPermissao, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 170, -1, -1));

        jComboBoxPermissao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        jComboBoxPermissao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPermissaoActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxPermissao, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 160, 210, 40));

        jLabelBairroPesquisa.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelBairroPesquisa.setForeground(new java.awt.Color(255, 255, 255));
        jLabelBairroPesquisa.setText("Pesquisar por");
        getContentPane().add(jLabelBairroPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, -1, -1));

        jComboBoxPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Usuario" }));
        jComboBoxPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBoxPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 220, 120, 30));

        jTextFieldPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jTextFieldPesquisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 220, 570, 30));

        jButtonPesquisar.setBackground(new java.awt.Color(204, 255, 204));
        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonPesquisar, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 220, 330, 30));

        jTableUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codusuario", "Usuário", "Senha", "Permissao"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableUsuariosMouseClicked(evt);
            }
        });
        jScrollPaneUsuarios.setViewportView(jTableUsuarios);
        if (jTableUsuarios.getColumnModel().getColumnCount() > 0) {
            jTableUsuarios.getColumnModel().getColumn(0).setResizable(false);
            jTableUsuarios.getColumnModel().getColumn(1).setResizable(false);
            jTableUsuarios.getColumnModel().getColumn(2).setResizable(false);
            jTableUsuarios.getColumnModel().getColumn(3).setResizable(false);
        }

        getContentPane().add(jScrollPaneUsuarios, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 260, 1170, 300));

        jButtonVoltar.setBackground(new java.awt.Color(255, 204, 204));
        jButtonVoltar.setText("Voltar");
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonVoltar, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 570, 330, 40));

        jButtonSalvar.setBackground(new java.awt.Color(204, 255, 204));
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 570, 330, 40));

        jButtonExcluir.setBackground(new java.awt.Color(255, 204, 204));
        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonExcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 570, 330, 40));

        jLabelPainel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Agenda-PainelFundo.png"))); // NOI18N
        getContentPane().add(jLabelPainel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 1300, 610));

        jLabelImagemFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AgendaFundo.png"))); // NOI18N
        getContentPane().add(jLabelImagemFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1300, 630));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxPermissaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPermissaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxPermissaoActionPerformed

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        
        controller.botaoVoltar();
        
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    private void jComboBoxPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxPesquisaActionPerformed

    private void jTextFieldPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldPesquisaActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed

        controller.botaoPesquisar();
        
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        
        controller.botaoSalvar();
        
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
        
        controller.botaoExcluir();
        
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jTableUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableUsuariosMouseClicked
        
        controller.mouseClicked();
        
    }//GEN-LAST:event_jTableUsuariosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaUsuario_2_AlterarExcluir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaUsuario_2_AlterarExcluir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaUsuario_2_AlterarExcluir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaUsuario_2_AlterarExcluir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaUsuario_2_AlterarExcluir().setVisible(true);
            }
        });
    }

    public JComboBox<Object> getjComboBoxPermissao() {
        return jComboBoxPermissao;
    }

    public void setjComboBoxPermissao(JComboBox<Object> jComboBoxPermissao) {
        this.jComboBoxPermissao = jComboBoxPermissao;
    }

    public JComboBox<Object> getjComboBoxPesquisa() {
        return jComboBoxPesquisa;
    }

    public void setjComboBoxPesquisa(JComboBox<Object> jComboBoxPesquisa) {
        this.jComboBoxPesquisa = jComboBoxPesquisa;
    }

    public JPasswordField getjPasswordFieldSenha() {
        return jPasswordFieldSenha;
    }

    public void setjPasswordFieldSenha(JPasswordField jPasswordFieldSenha) {
        this.jPasswordFieldSenha = jPasswordFieldSenha;
    }

    public JTable getjTableUsuarios() {
        return jTableUsuarios;
    }

    public void setjTableUsuarios(JTable jTableUsuarios) {
        this.jTableUsuarios = jTableUsuarios;
    }

    public JTextField getjTextFieldPesquisa() {
        return jTextFieldPesquisa;
    }

    public void setjTextFieldPesquisa(JTextField jTextFieldPesquisa) {
        this.jTextFieldPesquisa = jTextFieldPesquisa;
    }

    public JTextField getjTextFieldUsuario() {
        return jTextFieldUsuario;
    }

    public void setjTextFieldUsuario(JTextField jTextFieldUsuario) {
        this.jTextFieldUsuario = jTextFieldUsuario;
    }

    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JComboBox<Object> jComboBoxPermissao;
    private javax.swing.JComboBox<Object> jComboBoxPesquisa;
    private javax.swing.JLabel jLabelBairroPesquisa;
    private javax.swing.JLabel jLabelImagemFundo;
    private javax.swing.JLabel jLabelPainel;
    private javax.swing.JLabel jLabelPermissao;
    private javax.swing.JLabel jLabelSenha;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JPasswordField jPasswordFieldSenha;
    private javax.swing.JScrollPane jScrollPaneUsuarios;
    private javax.swing.JTable jTableUsuarios;
    private javax.swing.JTextField jTextFieldPesquisa;
    private javax.swing.JTextField jTextFieldUsuario;
    // End of variables declaration//GEN-END:variables
}
