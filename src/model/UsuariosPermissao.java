/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Rafael
 */
public class UsuariosPermissao {
    
    int codpermissao;
    String descricao;

    public int getCodpermissao() {
        return codpermissao;
    }

    public void setCodpermissao(int codpermissao) {
        this.codpermissao = codpermissao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    @Override
    public String toString() {
        return getDescricao(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
