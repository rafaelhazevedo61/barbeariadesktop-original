/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import util.Email;

/**
 *
 * @author Rafael
 */
public class TesteEnvioEmail {
    
    public static void main(String[] args) {
        
        ///CLASSE PARA TESTE DE ENVIO DE EMAIL
        
        String destinatario = "barbershop2021.java@gmail.com";
        
        Email envioEmailTeste = new Email("Assunto",
        "Informação corpo do email",
        destinatario);
        
        envioEmailTeste.enviar();
        
    }
    
}
